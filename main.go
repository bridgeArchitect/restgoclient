package main

import (
	"encoding/json"
	"fmt"
	"github.com/gotk3/gotk3/gtk"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
)

/* structure to save instance of "JSON's" value for function */
type Number struct {
	Value int `json:"Value"`
}

/* structure to save instance of "JSON's" writer */
type Writer struct {
	ID          string `json:"ID"`
	FirstName   string `json:"FirstName"`
	MiddleName  string `json:"MiddleName"`
	LastName    string `json:"LastName"`
	MainWork    string `json:"MainWork"`
	IDGenre     string `json:"IDGenre"`
}

/* variables for GTK-objects */
var (
	builder	   		 *gtk.Builder
	mainWindow 		 *gtk.Window
	functionButton   *gtk.Button
	tableButton      *gtk.Button
	recordButton     *gtk.Button
	valueEntry       *gtk.Entry
	answerTextBuffer *gtk.TextBuffer

)

/* function to handle error */
func handleError(err error) {

	/* handle error */
	if err != nil {
		log.Fatal(err)
	}

}

/* function to calculate remote function */
func calcFunction() {

	/* declaration of variables */
	var (
		resp       *http.Response
		err        error
		value      string
		connString string
		data       []byte
		number     Number
		answer     string
	)

	/* receive value */
	value, err = valueEntry.GetText()
	handleError(err)

	/* string for connection */
	connString = fmt.Sprintf("http://127.0.0.1:8000/F4/%s&3", value)

	/* call remote function */
	resp, err = http.Get(connString)
	handleError(err)

	/* read answer */
	data, err = ioutil.ReadAll(resp.Body)
	handleError(err)

	/* convert answer from JSON */
	err = json.Unmarshal(data, &number)
	handleError(err)

	/* write answer (value of function) */
	answer = "Answer: " + strconv.Itoa(number.Value)
	answerTextBuffer.SetText(answer)

}

/* function to get write with given id */
func getRecord() {

	/* declaration of variables */
	var (
		resp       *http.Response
		err        error
		value      string
		connString string
		data       []byte
		writer     Writer
		answer     string
	)

	/* receive id of writer */
	value, err = valueEntry.GetText()
	handleError(err)

	/* string for connection */
	connString = fmt.Sprintf("http://127.0.0.1:8000/Writer/%s", value)

	/* call remote function */
	resp, err = http.Get(connString)
	handleError(err)

	/* read answer */
	data, err = ioutil.ReadAll(resp.Body)
	handleError(err)

	/* convert answer from json */
	err = json.Unmarshal(data, &writer)
	handleError(err)

	/* write information about writer with given id */
	answer = fmt.Sprintf("%3s %15s %15s %15s %20s %3s\n", "ID", "First Name",
		"Middle Name", "Last Name", "Main Work", "IDg")
	answer += fmt.Sprintf("%3s %15s %15s %15s %20s %3s\n", writer.ID, writer.FirstName,
		writer.MiddleName, writer.LastName, writer.MainWork, writer.IDGenre)
	answerTextBuffer.SetText(answer)

}

/* function to get all writers */
func getTable() {

	/* declaration of variables */
	var (
		resp       *http.Response
		err        error
		data       []byte
		writers    []Writer
		i          int
		answer     string
	)

	/* call remote function */
	resp, err = http.Get("http://127.0.0.1:8000/Writer")
	handleError(err)

	/* read answer */
	data, err = ioutil.ReadAll(resp.Body)
	handleError(err)

	/* convert answer from json */
	err = json.Unmarshal(data, &writers)
	handleError(err)

	/* create row of answer (table of writers) */
	i = 0
	answer = fmt.Sprintf("%3s %15s %15s %15s %20s %3s\n", "ID", "First Name",
		"Middle Name", "Last Name", "Main Work", "IDg")
	for i < len(writers) {
		answer += fmt.Sprintf("%3s %15s %15s %15s %20s %3s\n", writers[i].ID, writers[i].FirstName,
			writers[i].MiddleName, writers[i].LastName, writers[i].MainWork, writers[i].IDGenre)
		i++
	}

	/* show answer */
	answerTextBuffer.SetText(answer)

}

/* function to make handlers */
func makeHandlers() {

	/* declaration of error variable */
	var (
		err error
	)

	/* make handlers for button */
	_, err = functionButton.Connect("clicked", calcFunction)
	handleError(err)
	_, err = tableButton.Connect("clicked", getTable)
	handleError(err)
	_, err = recordButton.Connect("clicked", getRecord)
	handleError(err)

}

/* function to create gtk-window */
func createWindow(nameGlade, nameWindow string) *gtk.Window {

	/* declaration of variables */
	var (
		win	*gtk.Window
		err error
	)

	/* add window from file */
	err = builder.AddFromFile(nameGlade)
	handleError(err)

	/* create window */
	obj, err := builder.GetObject(nameWindow)
	handleError(err)
	win = obj.(*gtk.Window)

	/* return window */
	return win

}

func createObject(nameObject string) interface{} {

	/* create gtk-object from builder */
	obj, err := builder.GetObject(nameObject)
	handleError(err)
	return obj

}

func main () {

	/* declaration of error variable */
	var (
		err error
	)

	/* create gtk-builder */
	gtk.Init(nil)
	builder, err = gtk.BuilderNew()
	handleError(err)

	/* create main windows with signal of destroying */
	mainWindow = createWindow("mainWindow.glade", "mainWindow")
	mainWindow.SetName("mainWindow")
	_, err = mainWindow.Connect("destroy", func() {
		gtk.MainQuit()
	})
	handleError(err)

	/* create objects of main window */
	valueEntry = createObject("valueEntry").(*gtk.Entry)
	functionButton = createObject("functionButton").(*gtk.Button)
	tableButton = createObject("tableButton").(*gtk.Button)
	recordButton = createObject("recordButton").(*gtk.Button)
	answerTextBuffer = createObject("answerTextBuffer").(*gtk.TextBuffer)

	/* make handlers for objects */
	makeHandlers()

	/* show main window */
	mainWindow.ShowAll()
	gtk.Main()

}
